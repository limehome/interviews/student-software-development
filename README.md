# Working Student Software Development (Javascript)

Thanks for taking the time to solve our challenge.

The task is designed to assess your interest, problem-solving skills and experience. We want to see your code, your approach, and your talent.

## Our Challenge

>We need a webpage that can search for properties, and allow the user to save their favourite properties (**wishlist**)

We expect most people to **spend between 4 and 6 hours** on this challenge, although you're free to use more time if you want.

So **keep it simple**, but also remember to **show off your skills**. We want to see what you can do.

We expect you to submit a **link to your code**(e.g. using Github, Gitlab, etc.). 


If you have any questions or feedback about the challenge, don't hesitate to reach out to us: [tech-hiring@limehome.com](tech-hiring@limehome.com)

Good luck with the challenge! We are looking forward to your solution!

---

## Requiremennts

We challenge you to build a **full prototype** with both UI and API.
The UI and API should be separated as client and server applications. 

### We expect a simple to use interface:


- the properties list should come from our [public API](https://api.limehome.com/properties/v1/public/properties).
- the property element with at least its name, one image and a button to favourite that property.
- If a property result has already been favorited, show with a different color its favourite button.
- A public **API** with two endpoints:
    - Save the favourite property into the DB
    - Get all the saved property for the user

   
![UI Example](./design/favorites-list.png)


### What we check:
- Functionality
- UI look and feel (minimal but appealing)
- API design (endpoint structure and request / response format)
- API robustness (e.g. validation)
- Code quality (e.g. code style and tests)

### Technology:
The front-end should be a single page app using a major framework (e.g. Angular, React or Vue).

Feel free to use one of our boilderplates:
- [React](https://gitlab.com/limehome/interviews/frontend-boilerplates/react-ui)
- [Angular](https://gitlab.com/limehome/interviews/frontend-boilerplates/angular-ui)

For the back-end we recommend JavaScript / TypeScript.